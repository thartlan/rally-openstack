# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import lzma
import requests
import tempfile

from rally.common import logging
from rally.task import validation

from rally_openstack import consts
from rally_openstack import scenario
from rally_openstack.scenarios.magnum import utils
from rally_openstack.services.image import image

LOG = logging.getLogger(__name__)

"""Scenarios for Kubernetes clusters with the latest Fedora CoreOS image and
   Kubernetes alpha/beta."""


@validation.add("enum", param_name="fcos_stream", values=["stable", "testing"])
@validation.add("required_services", services=[
    consts.Service.MAGNUM, consts.Service.GLANCE])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["magnum"]},
                    name="K8sLatest.create_cluster_latest",
                    platform="openstack")
class CreateClusterLatest(utils.MagnumScenario):
    def run(self, fcos_stream="stable", template_base={}, **kwargs):
        self.glance = image.Image(
            self._clients,
            name_generator=self.generate_random_name,
            atomic_inst=self.atomic_actions())

        # Get an image ID to use, and the latest kube tag
        latest_image_id = self.get_latest_image_id(fcos_stream)
        latest_kube_tag = self.get_latest_kube_tag()

        # Create a cluster template
        cluster_template = template_base
        cluster_template["image_id"] = latest_image_id
        cluster_template["labels"] = cluster_template.get("labels", {})
        cluster_template["labels"]["kube_tag"] = latest_kube_tag

        tmpl = self._create_cluster_template(**cluster_template)
        LOG.debug("Created cluster template {}".format(tmpl.name))

        cluster_template_id = tmpl.uuid

        # Create a cluster with the new template
        LOG.debug("Starting cluster creation")
        cluster = self._create_cluster(cluster_template_id, 1)
        LOG.debug("Created cluster {}, final state {}".format(
            cluster.name, cluster.status))

    def get_latest_image_id(self, stream):
        # Check the latest version that is available to download
        url = ("https://builds.coreos.fedoraproject.org"
               "/streams/{}.json".format(stream))
        resp = requests.get(url)
        self.assertEqual(
            resp.status_code,
            200,
            "bad response from {}".format(url))

        stream_json = resp.json()
        stream_openstack = stream_json["architectures"][
            "x86_64"]["artifacts"]["openstack"]
        release = stream_openstack["release"]

        # Name is always the same for a given stream and specific release.
        image_name = "fcos-{}-{}".format(stream, release)

        # If an image with this name already exists in glance,
        # just return the existing ID
        existing_images = self.glance.list_images()
        for img in existing_images:
            if img.name == image_name:
                LOG.debug("Found existing image for "
                          "fcos image {} with ID {}".format(
                              image_name, img.id))
                return img.id

        LOG.debug("Downloading new {} image {}".format(stream, image_name))

        image_location = stream_openstack["formats"][
            "qcow2.xz"]["disk"]["location"]
        image_req = requests.get(image_location)
        self.assertEqual(
            resp.status_code,
            200,
            "bad response from {}".format(image_location))

        # Decompress the image into a file on disk so it can be uploaded to
        # glance. Using tempfile ensures that the file is always cleaned up.
        with tempfile.NamedTemporaryFile(
                prefix=image_name, suffix=".qcow2") as image_file:
            LOG.debug("Writing decompressed image to {}".format(
                image_file.name))
            image_file.write(lzma.decompress(image_req.content))

            LOG.debug("Written decompressed image to disk, "
                      "uploading to glance now")

            image = self.glance.create_image(
                image_name=image_name,
                container_format="bare",
                image_location=image_file.name,
                disk_format="qcow2",
                properties={
                    "hw_rng_model": "virtio",
                    "os": "LINUX",
                    "os_distro": "fedora-coreos",
                }
            )

            LOG.debug("Created image {} ({})".format(image_name, image.id))

            return image.id

    def get_latest_kube_tag(self):
        # Checks the github tags API for kubernetes/kubernetes.
        url = "https://api.github.com/repos/kubernetes/kubernetes/tags"
        resp = requests.get(url)
        self.assertEqual(resp.status_code, 200,
                         "bad response from {}".format(url))

        tags_json = resp.json()

        # First tag in the list is the highest version.
        latest_tag = tags_json[0]

        LOG.debug("Latest kubernetes version is {}".format(latest_tag["name"]))

        return latest_tag["name"]
