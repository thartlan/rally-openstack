# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import yaml

from kubernetes import utils as k8sutils

from rally.common import logging
from rally.task import validation

from rally_openstack import consts
from rally_openstack import scenario
from rally_openstack.scenarios.magnum import utils

LOG = logging.getLogger(__name__)

"""Create jobs and check that they successfully run to completion"""


@validation.add("required_services", services=consts.Service.MAGNUM)
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(
    context={"cleanup@openstack": ["magnum.clusters", "nova.keypairs"]},
    name="K8sJobs.create_jobs", platform="openstack")
class CreateJobs(utils.MagnumScenario):
    def run(self, pre_apply, manifests):
        """create jobs and wait for them to complete.

        :param pre_apply: manifest yaml files to apply before creating jobs
        (e.g storage classes)
        :param manifests: manifest files used to create the jobs
        """

        k8s_client = self._get_k8s_api()

        for yaml_file in pre_apply:
            k8sutils.create_from_yaml(k8s_client, yaml_file)

        for manifest in manifests:
            with open(manifest, "r") as f:
                manifest_str = f.read()
            manifest = yaml.safe_load(manifest_str)
            job = self._create_job(manifest)
            msg = ("Job did not successfully complete")
            self.assertTrue(job, err_msg=msg)
