# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from rally.task import validation
from rally_openstack import consts
from rally_openstack import scenario
from rally_openstack.scenarios.magnum import utils

"""Scenarios for Swarm containers."""


@validation.add("required_services", services=[consts.Service.MAGNUM])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["magnum.clusters"]},
                    name="SwarmContainers.list_containers",
                    platform="openstack")
class ListContainers(utils.MagnumScenario):

    def run(self):
        """List all containers.

        """
        self._list_containers()


@validation.add("required_services", services=[consts.Service.MAGNUM])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["magnum.clusters"]},
                    name="SwarmContainers.create_and_list_containers",
                    platform="openstack")
class CreateAndListContainers(utils.MagnumScenario):

    def run(self, image, command):
        """create container and then list all containers.

        :param image: name or ID of the image for the container
        :param command: send command to the container
        """
        self._create_container(image, command)
        self._list_containers()


@validation.add("required_services", services=[consts.Service.MAGNUM])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["magnum.clusters"]},
                    name="SwarmContainers.create_stacks",
                    platform="openstack")
class CreateStacks(utils.MagnumScenario):

    def run(self, manifests):
        for manifest in manifests:
            self._create_stack(manifest)
